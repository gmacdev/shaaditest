package com.gmacv.shaaditest.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.gmacv.shaaditest.Database.UserDb;
import com.gmacv.shaaditest.Models.Results;
import com.gmacv.shaaditest.R;

import java.util.ArrayList;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static com.gmacv.shaaditest.Utils.Constants.state;
import static com.gmacv.shaaditest.Utils.Constants.username;

public class ResultsUserAdapter extends RecyclerView.Adapter<ResultsUserAdapter.PersonViewHolder> {

    private Context mContext;
    private ArrayList<Results> resultsArrayList;

    public ResultsUserAdapter(Context context, ArrayList<Results> resultsArrayList) {
        this.mContext = context;
        this.resultsArrayList = resultsArrayList;
    }

    @NonNull
    @Override
    public ResultsUserAdapter.PersonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview, parent, false);
        return new PersonViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ResultsUserAdapter.PersonViewHolder holder, final int position) {
//        Log.e("Size TO LOAD ", String.valueOf(resultsArrayList.size()));
        try {
            String name = resultsArrayList.get(position).getTitle() + " " + resultsArrayList.get(position).getFirst() + " " + resultsArrayList.get(position).getLast();
            String location = resultsArrayList.get(position).getCity() + ", " + resultsArrayList.get(position).getCountry();

            holder.name.setText(name);
            holder.age.setText(String.valueOf(resultsArrayList.get(position).age) + ",");
            holder.location.setText(location);
            Glide.with(mContext)
                    .load(resultsArrayList.get(position).photo)
                    .circleCrop()
                    .transition(withCrossFade())
                    .diskCacheStrategy(DiskCacheStrategy.DATA)
                    .into(holder.photo);
            Log.e("State + Position", resultsArrayList.get(position).getState() + ", " + position);
            if (resultsArrayList.get(position).getState() != -1) {
                switch (resultsArrayList.get(position).getState()) {
                    case 0:
                        Log.e("Case 0 for position", String.valueOf(position));
                        holder.decline.setVisibility(View.GONE);
                        holder.accept.setVisibility(View.GONE);
                        holder.declined.setVisibility(View.VISIBLE);
                        holder.accepted.setVisibility(View.GONE);
                        break;
                    case 1:
                        Log.e("Case 1 for position", String.valueOf(position));
                        holder.decline.setVisibility(View.GONE);
                        holder.accept.setVisibility(View.GONE);
                        holder.accepted.setVisibility(View.VISIBLE);
                        holder.declined.setVisibility(View.GONE);
                        break;
                }
            } else {
                holder.decline.setVisibility(View.VISIBLE);
                holder.accept.setVisibility(View.VISIBLE);
                holder.declined.setVisibility(View.GONE);
                holder.accepted.setVisibility(View.GONE);
            }
            holder.decline.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //add to DB
                    UserDb db = new UserDb(mContext);
                    db.updateIntegerValue(UserDb.TABLE_USERS, state, 0, username, resultsArrayList.get(position).username);
                    Log.e("Postion Clicked", String.valueOf(position));
                    resultsArrayList.get(position).setState(0);
//                    notifyDataSetChanged();
//                    holder.setIsRecyclable(false);
                    notifyItemChanged(position);
//                    resultsArrayList.clear();
//                    ((MainActivity)mContext).getnSetdata();
                }
            });
            holder.accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //add to DB
                    UserDb db = new UserDb(mContext);
                    db.updateIntegerValue(UserDb.TABLE_USERS, state, 1, username, resultsArrayList.get(position).username);
                    Log.e("Postion Clicked", String.valueOf(position));
                    resultsArrayList.get(position).setState(1);
//                    notifyDataSetChanged();
//                    holder.setIsRecyclable(false);
                    notifyItemChanged(position);
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return resultsArrayList.size();
    }

    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView age;
        TextView location;
        TextView declined;
        TextView accepted;
        ImageView photo;
        LinearLayout decline;
        LinearLayout accept;

        PersonViewHolder(final View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            age = itemView.findViewById(R.id.age);
            location = itemView.findViewById(R.id.location);
            declined = itemView.findViewById(R.id.declined);
            accepted = itemView.findViewById(R.id.accepted);
            photo = itemView.findViewById(R.id.photo);
            decline = itemView.findViewById(R.id.reject_layout);
            accept = itemView.findViewById(R.id.accept_layout);
        }
    }
}
