package com.gmacv.shaaditest.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by gulam on 5/12/17.
 */

public class SharedPref {
    private static final String TAG = "SharedPrefs";

    public static boolean preferenceFileExist(String fileName, Context context) {
        File f = new File("/data/data/" + context.getPackageName() + "/shared_prefs/" + fileName + ".xml");
        return f.exists();
    }

    public static void setString(Context context, String key, String value) {
        if (context != null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(TAG, Activity.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(key, value);
            editor.apply();
        }
    }

    public static String getString(Context context, String key) {
        String returnString = "";
        if (context != null) {
            SharedPreferences sp = context.getSharedPreferences(TAG, Activity.MODE_PRIVATE);
            returnString = sp.getString(key, "");
        }
        return returnString;
    }

    public static void setInt(Context context, String key, int value) {
        if (context != null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(TAG, Activity.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(key, value);
            editor.apply();
        }
    }

    public static int getInt(Context context, String key) {
        int returnString = 0;
        if (context != null) {
            SharedPreferences sp = context.getSharedPreferences(TAG, Activity.MODE_PRIVATE);
            returnString = sp.getInt(key, -1);
        }
        return returnString;
    }

    public static void setBoolean(Context context, String key, boolean value) {
        if (context != null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(TAG, Activity.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(key, value);
            editor.apply();
        }
    }

    public static Boolean getBoolean(Context context, String key) {
        boolean returnBoolean = false;
        if (context != null) {
            SharedPreferences sp = context.getSharedPreferences(TAG, Activity.MODE_PRIVATE);
            returnBoolean = sp.getBoolean(key, false);
        }
        return returnBoolean;
    }

    public static void saveMap(Context context, Map<String, String> inputMap, String keyMap) {
        SharedPreferences pSharedPref = context.getSharedPreferences(keyMap, Context.MODE_PRIVATE);
        if (pSharedPref != null) {
            JSONObject jsonObject = new JSONObject(inputMap);
            String jsonString = jsonObject.toString();
            SharedPreferences.Editor editor = pSharedPref.edit();
            editor.remove(keyMap).apply();
            editor.putString(keyMap, jsonString);
            editor.commit();
        }
    }

    public static Map<String, String> loadMap(Context context, String keyMap) {
        Map<String, String> outputMap = new HashMap<>();
        SharedPreferences pSharedPref = context.getSharedPreferences(keyMap, Context.MODE_PRIVATE);
        try {
            if (pSharedPref != null) {
                String jsonString = pSharedPref.getString(keyMap, (new JSONObject()).toString());
                JSONObject jsonObject = new JSONObject(jsonString);
                Iterator<String> keysItr = jsonObject.keys();
                while (keysItr.hasNext()) {
                    String key = keysItr.next();
                    String value = (String) jsonObject.get(key);
                    outputMap.put(key, value);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outputMap;
    }

    public static void removeString(Context context, String key) {
        if (context != null) {
            try {
                SharedPreferences sharedPreferences = context.getSharedPreferences(TAG, Activity.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.remove(key);
                editor.apply();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void clearAll(Context context) {
        if (context != null) {
            try {
                SharedPreferences sharedPreferences = context.getSharedPreferences(TAG, Activity.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.clear();
                editor.apply();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
