package com.gmacv.shaaditest.Utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Constants {
    public static final String id = "id";
    public static final String loadingUrl = "https://acegif.com/wp-content/uploads/loading-14.gif";
    public static final int SERVER_TIMEOUT_ATTEMPT = 5;
    public static final int SERVER_TIMEOUT = 5000;
    public static final String BASE_URL = "https://randomuser.me/api/?results=10";
    public static final String RESULTS = "results";
    public static final String username = "username";
    public static final String gender = "gender";
    public static final String title = "title";
    public static final String photo = "photo";
    public static final String age = "age";
    public static final String city = "city";
    public static final String country = "country";
    public static final String version = "version";
    public static final String info = "info";
    public static final String state = "state";
    public static final String name = "name";
    public static final String location = "location";
    public static final String picture = "picture";
    public static final String dob = "dob";
    public static final String login = "login";
    public static final String medium = "medium";
    public static final String first = "first";
    public static final String last = "last";
    public static String versionSP;

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
