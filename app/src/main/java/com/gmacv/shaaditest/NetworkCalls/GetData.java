package com.gmacv.shaaditest.NetworkCalls;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.gmacv.shaaditest.Activities.MainActivity;
import com.gmacv.shaaditest.Database.UserDb;
import com.gmacv.shaaditest.Models.Results;
import com.gmacv.shaaditest.Utils.SharedPref;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import static com.gmacv.shaaditest.Utils.Constants.BASE_URL;
import static com.gmacv.shaaditest.Utils.Constants.RESULTS;
import static com.gmacv.shaaditest.Utils.Constants.SERVER_TIMEOUT;
import static com.gmacv.shaaditest.Utils.Constants.SERVER_TIMEOUT_ATTEMPT;
import static com.gmacv.shaaditest.Utils.Constants.info;
import static com.gmacv.shaaditest.Utils.Constants.version;
import static com.gmacv.shaaditest.Utils.Constants.versionSP;

public class GetData {

    private UserDb db;
    private RequestQueue requestQueue;
    private Context mContext;

    public GetData(Context context) {
        mContext = context;
        requestQueue = Volley.newRequestQueue(mContext);
        db = new UserDb(mContext);
    }

    public void apiCall() {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, BASE_URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("API response ", response.toString());
                try {
                    if (!SharedPref.preferenceFileExist(versionSP, mContext)) {
                        SharedPref.setString(mContext, versionSP, response.getJSONObject(info).getString(version));
                        JSONArray results = response.getJSONArray(RESULTS);
                        String resultsString = results.toString();
                        addResults(resultsString);
                    } else {
                        int lastUsedVersion = Integer.parseInt(SharedPref.getString(mContext, versionSP));
                        int currentVersion = Integer.parseInt(response.getJSONObject(info).getString(version));
                        if (currentVersion > lastUsedVersion) {
                            SharedPref.setString(mContext, versionSP, response.getJSONObject(info).getString(version));
                            JSONArray results = response.getJSONArray(RESULTS);
                            String resultsString = results.toString();
                            addResults(resultsString);
                        }
                    }
                    ((MainActivity) mContext).getnSetdata();
                    ((MainActivity) mContext).hideLoader();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e("Error Response", String.valueOf(networkResponse));
                //can log it to firebase
            }
        });

        jsonObjectRequest.setRetryPolicy(new
                DefaultRetryPolicy(SERVER_TIMEOUT,
                SERVER_TIMEOUT_ATTEMPT,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    private void addResults(String resultsString) {
//        ReponseModel reponseModels = new Gson().fromJson(resultsString,ReponseModel.class);
//        ArrayList<ReponseModel> reponseModelArrayList = new ArrayList<>(Arrays.asList(reponseModels));
//        db.insertResults(reponseModelArrayList);
        Results[] results = new Gson().fromJson(resultsString, Results[].class);
        ArrayList<Results> resultsArrayList = new ArrayList<>(Arrays.asList(results));
        db.insertResults(resultsArrayList);
    }
}
