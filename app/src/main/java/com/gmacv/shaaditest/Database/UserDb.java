package com.gmacv.shaaditest.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.gmacv.shaaditest.Models.Results;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.ArrayList;

import static com.gmacv.shaaditest.Utils.Constants.age;
import static com.gmacv.shaaditest.Utils.Constants.city;
import static com.gmacv.shaaditest.Utils.Constants.country;
import static com.gmacv.shaaditest.Utils.Constants.first;
import static com.gmacv.shaaditest.Utils.Constants.gender;
import static com.gmacv.shaaditest.Utils.Constants.last;
import static com.gmacv.shaaditest.Utils.Constants.photo;
import static com.gmacv.shaaditest.Utils.Constants.state;
import static com.gmacv.shaaditest.Utils.Constants.title;
import static com.gmacv.shaaditest.Utils.Constants.username;

public class UserDb extends SQLiteOpenHelper {
    public static final String TABLE_USERS = "users";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "users.db";
    private Context mContext;
    private String CREATE_TABLE_USERS = "CREATE TABLE IF NOT EXISTS " + TABLE_USERS +
            "(" +
            username + " VARCHAR(255) PRIMARY KEY," +
            gender + " VARCHAR(255)," +
            title + " VARCHAR(255)," +
            first + " VARCHAR(255)," +
            last + " VARCHAR(255)," +
            photo + " VARCHAR(255)," +
            age + " INTEGER," +
            city + " VARCHAR(255)," +
            country + " VARCHAR(255)," +
            state + " INTEGER DEFAULT -1"
            + ")";

    public UserDb(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(CREATE_TABLE_USERS);
        } catch (SQLiteException e) {
            e.getStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
        onCreate(db);
    }

    public void insertResults(ArrayList<Results> resultsArrayList) {
        Log.e("Now", "inserting results " + resultsArrayList);
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        for (Results currentResult : resultsArrayList) {
            values.put(gender, currentResult.gender);
            Log.e("Now", "gender inserted " + currentResult.gender);
            values.put(username, currentResult.getLoginModel().username);
            values.put(title, currentResult.getNameModel().title);
            values.put(first, currentResult.getNameModel().first);
            values.put(last, currentResult.getNameModel().last);
            values.put(photo, currentResult.getPictureModel().photo);
            values.put(age, currentResult.getDobModel().age);
            values.put(city, currentResult.getLocationModel().city);
            values.put(country, currentResult.getLocationModel().country);
            db.insertWithOnConflict(TABLE_USERS, null, values, SQLiteDatabase.CONFLICT_IGNORE);
        }
    }

    public ArrayList<Results> getAllResultsModel() {
        SQLiteDatabase db = this.getWritableDatabase();
        JSONObject rowObject = null;
        ArrayList<Results> informationModelArrayList = new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_USERS, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            int totalColumn = cursor.getColumnCount();
            rowObject = new JSONObject();
            for (int i = 0; i < totalColumn; i++) {
                if (cursor.getColumnName(i) != null) {
                    try {
                        if (cursor.getString(i) != null) {
                            Log.d("TAG_NAME", cursor.getString(i));
                            rowObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            rowObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                        Log.d("TAG_NAME", e.getMessage());
                    }
                }
            }
            Gson gson = new GsonBuilder().create();
            Results informationModel = gson.fromJson(rowObject.toString(), Results.class);
            informationModelArrayList.add(informationModel);
            cursor.moveToNext();

        }
        cursor.close();
        return informationModelArrayList;
    }


    public void updateStringValue(String tableName, String columnName, String newValue, String whereTag, String whereValue) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(columnName, newValue);
        db.update(tableName, cv, whereTag + " =?", new String[]{whereValue});
        db.close();
    }

    public void updateIntegerValue(String tableName, String columnName, int newValue, String whereTag, String whereValue) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(columnName, newValue);
        db.update(tableName, cv, whereTag + " =?", new String[]{whereValue});
        db.close();
    }
}
