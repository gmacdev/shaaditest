package com.gmacv.shaaditest.Models;

import com.gmacv.shaaditest.Utils.Constants;
import com.google.gson.annotations.SerializedName;

public class NameModel {

    @SerializedName(Constants.title)
    public String title;
    @SerializedName(Constants.first)
    public String first;
    @SerializedName(Constants.last)
    public String last;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }
}
