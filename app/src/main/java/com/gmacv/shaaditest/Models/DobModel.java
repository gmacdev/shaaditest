package com.gmacv.shaaditest.Models;

import com.gmacv.shaaditest.Utils.Constants;
import com.google.gson.annotations.SerializedName;

public class DobModel {
    @SerializedName(Constants.age)
    public int age;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
