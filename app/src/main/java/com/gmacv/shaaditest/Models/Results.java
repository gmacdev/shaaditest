package com.gmacv.shaaditest.Models;

import com.gmacv.shaaditest.Utils.Constants;
import com.google.gson.annotations.SerializedName;

public class Results {

    @SerializedName(Constants.gender)
    public String gender;
    @SerializedName(Constants.name)
    public NameModel nameModel;
    @SerializedName(Constants.location)
    public LocationModel locationModel;
    @SerializedName(Constants.picture)
    public PictureModel pictureModel;
    @SerializedName(Constants.dob)
    public DobModel dobModel;
    @SerializedName(Constants.login)
    public LoginModel loginModel;
    public String first;
    public String photo;
    public String title;
    public String last;
    public String username;
    public String city;
    public String country;
    public int state;
    public int age;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public NameModel getNameModel() {
        return nameModel;
    }

    public void setNameModel(NameModel nameModel) {
        this.nameModel = nameModel;
    }

    public LocationModel getLocationModel() {
        return locationModel;
    }

    public void setLocationModel(LocationModel locationModel) {
        this.locationModel = locationModel;
    }

    public PictureModel getPictureModel() {
        return pictureModel;
    }

    public void setPictureModel(PictureModel pictureModel) {
        this.pictureModel = pictureModel;
    }

    public DobModel getDobModel() {
        return dobModel;
    }

    public void setDobModel(DobModel dobModel) {
        this.dobModel = dobModel;
    }

    public LoginModel getLoginModel() {
        return loginModel;
    }

    public void setLoginModel(LoginModel loginModel) {
        this.loginModel = loginModel;
    }
}
