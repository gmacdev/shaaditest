package com.gmacv.shaaditest.Models;

import com.gmacv.shaaditest.Utils.Constants;
import com.google.gson.annotations.SerializedName;

public class LoginModel {
    @SerializedName(Constants.username)
    public String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
