package com.gmacv.shaaditest.Models;

import com.gmacv.shaaditest.Utils.Constants;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReponseModel {

    @SerializedName(Constants.RESULTS)
    private List<Results> resultsList;

    public List<Results> getResultsList() {
        return resultsList;
    }

    public void setResultsList(List<Results> resultsList) {
        this.resultsList = resultsList;
    }
}
