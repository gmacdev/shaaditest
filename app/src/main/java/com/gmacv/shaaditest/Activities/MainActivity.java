package com.gmacv.shaaditest.Activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.stetho.Stetho;
import com.gmacv.shaaditest.Adapter.ResultsUserAdapter;
import com.gmacv.shaaditest.Database.UserDb;
import com.gmacv.shaaditest.Models.Results;
import com.gmacv.shaaditest.NetworkCalls.GetData;
import com.gmacv.shaaditest.R;
import com.gmacv.shaaditest.Utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.loadingIV)
    ImageView loadingIV;
    @BindView(R.id.rv)
    RecyclerView recyclerView;
    ArrayList<Results> allResultsModel = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Stetho.initializeWithDefaults(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LoadData();
    }

    private void LoadData() {
        loadingIV.setVisibility(View.VISIBLE);
        Glide.with(this).asGif().load(Constants.loadingUrl).diskCacheStrategy(DiskCacheStrategy.DATA).into(loadingIV);
        //if net is available then only do this
        if (Constants.isNetworkAvailable(this)) {
            GetData getData = new GetData(this);
            getData.apiCall();
        } else {
            hideLoader();
            getnSetdata();
        }
    }

    public void getnSetdata() {
        //get all data from database
        UserDb db = new UserDb(getApplicationContext());
        allResultsModel.clear();
        allResultsModel = db.getAllResultsModel();
        ResultsUserAdapter resultsUserAdapter = new ResultsUserAdapter(this, allResultsModel);
        //Load Recycler view if data is available
        if (allResultsModel.size() > 0) {
            recyclerView.setAdapter(resultsUserAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            recyclerView.setHasFixedSize(true);
            resultsUserAdapter.notifyDataSetChanged();
        } else {
            //show alert "no local data available, turn on internet and relaunch the app"
        }
    }

    public void hideLoader() {
        loadingIV.setVisibility(View.GONE);
    }
}